package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

public class MyTreeParser extends TreeParser {

	tb.antlr.symbolTable.GlobalSymbols globalVariables = new tb.antlr.symbolTable.GlobalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected void declareGlobal(String variableName) {
		drukuj("Zmienna " + variableName + " zadeklarowana");
		globalVariables.newSymbol(variableName);
	}
	
	protected void assignToGlobal(String variableName, Integer varValue) {
		drukuj("Przypisano " + varValue + " do " + variableName);
		globalVariables.setSymbol(variableName, varValue);
	}
	
	protected Integer getVar(String name) {
		return globalVariables.getSymbol(name);
	}
}
