tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : 
	(block | toPrint | global_var | set_var | expr)*;
	//PRINT (e1=expr {drukuj ($e1.text + " = " + $e1.out.toString()); })+
	//| ( e=expr {drukuj ($e.text + " = " + $e.out.toString());})* ;
	
toPrint:
  ^(PRINT e=expr){drukuj ($e.text + " = " + $e.out.toString());}
  //| ^(PRINT id=ID){drukuj(getVar($id.text) + "");}
  ;
  
global_var:
  ^(VAR id=ID val=expr){declareGlobal($id.text);}
  ;
  
set_var:
  ^(PODST id=ID val=expr){assignToGlobal($id.text, $val.out);}
  ;
  
block:
  ^(BB val=block_interior){drukuj($val.text);}
  ;
  
block_interior
    : (toPrint | global_var | set_var | expr)* 
    ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;}
        | ^(MOD   e1=expr e2=expr) {$out = $e1.out \% $e2.out;}
        //| ^(PODST i1=ID   e2=expr)
        | ID                       {$out = getVar($ID.text);}
        | INT                      {$out = getInt($INT.text);}
        ;
