grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (block | stat )+ EOF!;

stat
    : expr NL -> expr

    | VAR ID NL -> ^(VAR ID)
    | PRINT expr NL -> ^(PRINT expr)
    | ID PODST expr NL -> ^(PODST ID expr)
    | IF expr block -> ^(IF expr block)
    | NL ->
    ;

block
    : BB^ (block | stat)* BE!
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      | MOD^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;
    

VAR :'var';

PRINT: 'print';

BLOCK
  : 'BLOCK';
  
IF
  : 'IF';
  
ENDIF
  : 'ENDIF';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;
  
BB // block begin
  : '{'
  ;
 
BE // block end
  : '}'
  ;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;

MOD
  : '%'
  ;